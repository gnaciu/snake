This is my first RUST project. A simple snake game where you have to catch food to grow.

To start go to /main and run command "cargo run".

Use arrows to move.

screenshots:

![snake](snake1.png)

![snake](snake2.png)

![snake](snake3.png)

![snake](snake4.png)
