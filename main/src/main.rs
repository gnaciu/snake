extern crate piston;
extern crate graphics;
extern crate glutin_window;
extern crate opengl_graphics;
extern crate piston_window;

use piston_window::draw_state::Blend;
use piston_window::*;
use std::io;
use std::io::Write;
use std::thread;
use std::time;
use termion;
use termion::input::TermRead;
use termion::raw::IntoRawMode;
use piston::window::WindowSettings;
use piston::event_loop::*;
use piston::input::*;
use glutin_window::GlutinWindow as Window;
use opengl_graphics::{ GlGraphics, OpenGL };
use rand::Rng;
use std::collections::LinkedList;

const initial_length: u16 = 5;
const growth_length: u16 = 5;
const colour_high_limit: f32 = 1.0;
const colour_low_limit: f32 = 0.0;
const bug_num: u8 = 5;
const bounds: f64 = 500.0;

#[derive(Clone, Copy)]
pub enum Direction{
    Up,
    Down,
    Left,
    Right
}
#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Colour {
    colour : [f32; 4]
}
impl Colour{
    fn change(&mut self){
        let mut rng = rand::thread_rng();
        let index = rng.gen_range(0, 3);
        let add = rng.gen_bool(0.5);
        if add {self.colour[index] +=0.025;}
        else { self.colour[index] -=0.025;}
        if self.colour[index] >= 1.1{ self.colour[index] = 1.0;}
        if self.colour[index] <= 0.0{ self.colour[index] = 0.1;}
    }
}


#[derive(Debug, PartialEq, Clone, Copy)]
 pub struct Point {
     x: f64,
     y: f64,
     size: f64,
     colour: Colour
}
impl Point {
    fn augment_x(&mut self, change: f64){ self.x += change; }
    fn augment_y(&mut self, change: f64){ self.y += change; }
}
pub struct Snake {
    head: Head,
    body: LinkedList<Point>,
}
impl Snake {
    fn update_position(&mut self){
        if let Some(mut head) = self.body.back_mut(){
            match self.head.direction {
                Direction::Up => {
                    head.augment_y(-1.0); }
                Direction::Down => { head.augment_y(1.0); }
                Direction::Left => { head.augment_x(-1.0); }
                Direction::Right => { head.augment_x(1.0); }
            }
        }
    }
    fn grow(&mut self){
        for i in 0..growth_length {
            if let Some(tail) = self.body.front().cloned(){
                self.body.push_front( tail)
            }
        }
    }
    fn move_head(&mut self){
        if let Some(mut new_head) = self.body.back().cloned() {
            new_head.colour.change();
            self.body.push_back(new_head);
            self.body.pop_front();
        }
        self.update_position()
    }
    fn append_head(&mut self){
        if let Some(new_head) = self.body.back().cloned() {
            self.body.push_back(new_head);
        }
        self.update_position()
    }
}

pub struct Head {
    direction: Direction
}
impl Head {
    fn update_direction(&mut self,  key: &termion::event::Key) {
        match key {
            termion::event::Key::Up => { self.direction = Direction::Up;}
            termion::event::Key::Down => { self.direction = Direction::Down;}
            termion::event::Key::Left => { self.direction = Direction::Left;}
            termion::event::Key::Right => { self.direction = Direction::Right;}
            _ => {}
        }
    }
    fn chnage_direction(&mut self, dir: Direction){
        self.direction = dir;
    }
}


pub struct Food {
    point: Point,
    head: Head
}
impl Food {
    fn respawn(&mut self) {
        let mut rng = rand::thread_rng();
        self.point = generate_random_point();
        self.head.direction = generate_random_direction();
    }
    fn run (&mut self){
        let mut rng = rand::thread_rng();
        let change_direction = rng.gen_bool(0.05);
        if change_direction{
            self.head.direction = generate_random_direction();
        }
        self.update_location();
        self.point.colour.change()

    }
    fn update_location(&mut self){
        match self.head.direction {
            Direction::Up => { self.point.augment_y(-0.5); }
            Direction::Down => { self.point.augment_y(0.5); }
            Direction::Left => { self.point.augment_x(-0.5); }
            Direction::Right => { self.point.augment_x(0.5); }
        }
    }
}



pub struct Game{
    snake: Snake,
    bugs: Vec<Food>,
}
impl Game{
   fn is_food_eaten(&mut self){
       for bug in self.bugs.iter_mut() {
           let dist: f64 = calculate_distance(&mut bug.point, &mut self.snake.body.back_mut().unwrap());
           if dist <= 10.0 {
               bug.respawn();
               self.snake.grow();
           } else {
               bug.run()
           }
       }
    }
}


pub struct App {
    gl: GlGraphics, // OpenGL drawing backend.
}

impl App {
    fn render(&mut self, args: &RenderArgs, game: &mut Game) {
        use graphics::*;
        // Clear the screen.
        const GREEN: [f32; 4] = [0.0, 0.0, 0.0, 1.0];
        self.gl.draw(args.viewport(), |c, gl| {
        clear(GREEN, gl);
        for bod in &mut game.snake.body {
            let square = rectangle::square(bod.x, bod.y, bod.size);
            let (x, y) = (args.window_size[0] / 2.0,
                      args.window_size[1] / 2.0);
            let transform = c.transform.trans(x, y)
                .trans(-25.0, -25.0);

            // Draw a box rotating around the middle of the screen.
            ellipse(bod.colour.colour, square, transform, gl);
        }
            //for i in bug_num{
         for bug in  &mut game.bugs{
                    let square = rectangle::square(bug.point.x, bug.point.y, bug.point.size);
                    let (x, y) = (args.window_size[0] / 2.0,
                                  args.window_size[1] / 2.0);
                    let transform = c.transform.trans(x, y)
                        .trans(-25.0, -25.0);

                    // Draw a box rotating around the middle of the screen.
                    ellipse(bug.point.colour.colour, square, transform, gl);

         }
        });
    }
    }

fn main() {
    let mut rng = rand::thread_rng();

    // Set terminal to raw mode to allow reading stdin one key at a time
    let mut stdout = io::stdout().into_raw_mode().unwrap();
    // Use asynchronous stdin
    let mut stdin = termion::async_stdin().keys();


    //spinning cube code as an example
    // Change this to OpenGL::V2_1 if not working.
    let opengl = OpenGL::V3_2;

    // Create an Glutin window.
    let mut window: PistonWindow = WindowSettings::new(
        "spinning-square",
        [1000, 1000]
    )
        .graphics_api(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();
    //Initialize the snake
    let mut head: Head = Head { direction : Direction::Up };
    let mut body = generate_body();
    let mut snake: Snake = Snake {head: head, body: body};
    let mut bugs: Vec<Food> = generate_bugs();
    let mut game: Game = Game {snake: snake, bugs: bugs};
    // Create a new game and run it.
    let mut app = App {
        gl: GlGraphics::new(opengl),
    };

    //let mut events = Events::new(EventSettings::new());
    //while let Some(e) = events.next(&mut window)
    while let Some(e) = window.next(){

        //        // Read input (if any)
        //let inp : Button::Keyboard = e.press_args();
      //  game.snake.head.update_direction(&inp);
        if let Some(Button::Keyboard(Key::Up)) = e.press_args() {
            game.snake.head.chnage_direction(Direction::Up)
        }
        if let Some(Button::Keyboard(Key::Down)) = e.press_args() {
            game.snake.head.chnage_direction(Direction::Down)
        }
        if let Some(Button::Keyboard(Key::Left)) = e.press_args() {
            game.snake.head.chnage_direction(Direction::Left)
        }
        if let Some(Button::Keyboard(Key::Right)) = e.press_args() {
            game.snake.head.chnage_direction(Direction::Right)
        }
        game.snake.move_head();
        game.is_food_eaten();
        if let Some(r) = e.render_args() { app.render(&r, &mut game); }
    }

}





//helper functions

fn generate_body() -> LinkedList<Point> {
    let mut body: LinkedList<Point> = LinkedList::new();
    for i in 0..initial_length {
        body.push_back(generate_body_point())
    }
    return body;
}
fn generate_bugs() -> Vec<Food>{
        let mut bugs : Vec<Food> = Vec::new();
    for i in 0..bug_num {
       let mut bug: Food = Food {point: generate_body_point(), head: Head{direction: generate_random_direction()} };
        bugs.push(bug);
    }
    return bugs;
}
fn generate_body_point() -> Point{
    let mut point: Point = Point {x: 0.0,y: 0.0, size:10.0,  colour: Colour {colour: [1.0, 3.0, 0.0, 1.0]}};
    return point;
}
fn generate_random_colour() -> Colour{
    let mut rng = rand::thread_rng();
    let mut colour: Colour = Colour { colour :
        [rng.gen_range(colour_low_limit, colour_high_limit),
        rng.gen_range(colour_low_limit, colour_high_limit),
        rng.gen_range(colour_low_limit, colour_high_limit), 1.0]};
return colour;
}
fn generate_random_point() -> Point{
    let mut rng = rand::thread_rng();
    let mut point: Point = Point { x: rng.gen_range(-200.0, 200.0), y: rng.gen_range(-200.0, 200.0),
        size: 5.0,  colour: generate_random_colour()};
return point;

}
fn generate_random_direction() -> Direction{
    let mut rng = rand::thread_rng();
    let mut num: u8 = 0;
    let num = rng.gen_range(0, 4);
    match num {
        0 =>{ return Direction::Up}
        1 =>{ return Direction::Down }
        2 => {return Direction::Left }
        3 =>{ return Direction::Right }
        _ => {return Direction::Down}
    }
}

fn calculate_distance(point_1: &mut Point,  point_2:&mut  Point) -> f64{
    let mut stdout = io::stdout().into_raw_mode().unwrap();
    let distance :f64 = ((point_2.x - point_1.x).powf(2.0)+(point_2.y - point_1.y).powf(2.0)).powf(0.5);

    return distance;
}
